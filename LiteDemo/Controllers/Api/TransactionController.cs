﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using LiteDemo.Data;
using LiteDemo.Data.Models;

namespace LiteDemo.Controllers.Api
{
    public class TransactionController : ApiController
    {
        private DB db = new DB();

        // GET: api/Transaction
        public IQueryable<TransactionModel> GetTransactions()
        {
            return db.Transactions;
        }

        // GET: api/Transaction/5
        [ResponseType(typeof(TransactionModel))]
        public IHttpActionResult GetTransactionModel(int id)
        {
            TransactionModel transactionModel = db.Transactions.Find(id);
            if (transactionModel == null)
            {
                return NotFound();
            }

            return Ok(transactionModel);
        }

        // PUT: api/Transaction/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTransactionModel(TransactionModel transactionModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            /*if (id != transactionModel.Id)
            {
                return BadRequest();
            }*/

            db.Entry(transactionModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransactionModelExists(transactionModel.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Transaction
        [ResponseType(typeof(TransactionModel))]
        public IHttpActionResult PostTransactionModel(TransactionModel transactionModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Transactions.Add(transactionModel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = transactionModel.Id }, transactionModel);
        }

        // DELETE: api/Transaction/5
        [ResponseType(typeof(TransactionModel))]
        public IHttpActionResult DeleteTransactionModel(int id)
        {
            TransactionModel transactionModel = db.Transactions.Find(id);
            if (transactionModel == null)
            {
                return NotFound();
            }

            db.Transactions.Remove(transactionModel);
            db.SaveChanges();

            return Ok(transactionModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TransactionModelExists(int id)
        {
            return db.Transactions.Count(e => e.Id == id) > 0;
        }
    }
}