﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

using LiteDemo.Data.Models;
using System.Data.Entity.Migrations;

namespace LiteDemo.Data
{
    public class DB: DbContext
    {
        public DbSet<TransactionModel> Transactions { get; set; }
    }

    public class InitData : DBInit<DB>
    {

    }

    public class DBInit<TContext> : IDatabaseInitializer<TContext> where TContext : System.Data.Entity.DbContext
    {
        public DBInit()
        {
        }

        public void InitializeDatabase(TContext context)
        {
            var configuration = new DbMigrationsConfiguration<TContext>();
            configuration.AutomaticMigrationDataLossAllowed = true;
            configuration.AutomaticMigrationsEnabled = true;
            var migrator = new DbMigrator(configuration);
            migrator.Update();
        }

    }

}