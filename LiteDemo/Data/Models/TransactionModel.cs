﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LiteDemo.Data.Models
{
    public class TransactionModel
    {
        public int Id { get; set; }
        public string MerchantName { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public string Decision { get; set; }
        public string EntryMode { get; set; }
    }
}