﻿(function () {
    'use strict';

    angular.module('app', [
        // Angular modules 
        'ngRoute', 'ngResource',

        // Custom modules 

        // 3rd Party Modules
        'angularMoment',

    ]).filter("mydate", function () {
        var re = /\\\/Date\(([0-9]*)\)\\\//;
        return function (x) {
            console.log(1);
            var m = x.match(re);
            if (m) return new Date(parseInt(m[1]));
            else 
                return null;
        };
    }).directive('moDateInput', function ($window) {
        return {
            require: '^ngModel',
            restrict: 'A',
            link: function (scope, elm, attrs, ctrl) {
                var moment = $window.moment;
                var dateFormat = attrs.moDateInput;
                attrs.$observe('moDateInput', function (newValue) {
                    if (dateFormat == newValue || !ctrl.$modelValue) return;
                    dateFormat = newValue;
                    ctrl.$modelValue = new Date(ctrl.$setViewValue);
                });

                ctrl.$formatters.unshift(function (modelValue) {
                    if (!dateFormat || !modelValue) return "";
                    var retVal = moment(modelValue).format(dateFormat);
                    return retVal;
                });

                ctrl.$parsers.unshift(function (viewValue) {
                    var date = moment(viewValue, dateFormat);
                    return date.format("YYYY-MM-DDTHH:mm:ss.SSSZZ");
                });
            }
        };
    });

})();
