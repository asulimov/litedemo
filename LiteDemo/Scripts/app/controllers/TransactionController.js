﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('TransactionController', TransactionController);

    TransactionController.$inject = ['$scope', 'TransactionService'];

    function TransactionController($scope, TransactionService) {
        $scope.refresh = function () {
            $scope.transactions = TransactionService.query({}, function (e) {
            });
        };

        $scope.refreshItem = function (id) {
            TransactionService.get({ id: id}, function (e) {
                $.each($scope.transactions, function (idx, elem) {
                    if (elem.Id == e.Id)
                        $scope.transactions[idx] = e;
                });
            });
        };

        $scope.transaction = {
            Id: 0,
            MerchantName: 'MerchantName',
            Date: new Date(),
            Amount: 0,
            Decision: 'Decision',
            EntryMode: 'EntryMode',
        };

        $scope.add = function () {
            TransactionService.post( $scope.transaction, function () {
                $('#addTransaction').modal('toggle')
                $scope.refresh();
            });
        };

        $scope.delete = function (transaction) {
            bootbox.confirm("Are you sure delete? Id: " + transaction.Id, function (result) {                
                TransactionService.delete({ id: transaction.Id }, function () {
                    $scope.refresh();
                });
            });
        };

        $scope.editable = function (transaction) {
            transaction.editable = !transaction.editable;
            if (!transaction.editable)
                $scope.save(transaction);
        };

        $scope.save = function (transaction) {
            var id = transaction.Id;
            transaction.editable = false;
            TransactionService.put(transaction, function () {
                $scope.refreshItem(id);
            });
        };
    }
})();
