﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('TransactionService', TransactionService);

    TransactionService.$inject = ['$resource'];

    function TransactionService($resource) {
        return $resource('api/Transaction/', {}, {
            query: { method: 'GET', params: {}, isArray: true },
            get: { method: 'GET', params: {}, isArray: false },
            post: { method: 'POST', params: {}, isArray: false },
            put: { method: 'PUT', params: {}, isArray: false },
            delete: { method: 'DELETE', params: {}, isArray: false },
        });
    }
})();